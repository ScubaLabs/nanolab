﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.IO;

public static class ConfigReader
{
    public static string[] Atoms { get { return atoms; } }
    private static readonly string[] atoms;

    public static Dictionary<string, int>[] Molecules { get { return molecules; } }
    private static readonly Dictionary<string, int>[] molecules;

    static ConfigReader()
    {
        atoms = ParseAtoms();
        molecules = ParseMolecules();
    }

    private static string[] ParseAtoms()
    {
        TextAsset atomsText = (TextAsset)Resources.Load("Atoms", typeof(TextAsset));
        return atomsText.text.Split(Environment.NewLine.ToCharArray())
            .Where(a => !string.IsNullOrEmpty(a))
            .ToArray();
    }

    private static Dictionary<string, int>[] ParseMolecules()
    {
        TextAsset moleculesText = (TextAsset)Resources.Load("Molecules", typeof(TextAsset));
        using (var sr = new StringReader(moleculesText.text))
        {
            string currString;
            var result = new List<Dictionary<string, int>>();
            var currDict = new Dictionary<string, int>();
            while ((currString = sr.ReadLine()) != null)
            {
                if (string.IsNullOrEmpty(currString))
                {
                    result.Add(currDict);
                    currDict = new Dictionary<string, int>();
                }
                else
                {
                    if (currDict.ContainsKey(currString))
                        ++currDict[currString];
                    else
                        currDict.Add(currString, 1);
                }
            }
            if (currDict.Count != 0)
                result.Add(currDict);
            return result.ToArray();
        }
    }
}
