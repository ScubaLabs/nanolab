﻿using UnityEngine;
using System;
using System.Collections.Generic;

using AtomAndEmissionColor = System.Collections.Generic.KeyValuePair<UnityEngine.Color, UnityEngine.Color>;

[Serializable]
public enum AtomType { hydrogen, oxygen, carbon, nitrogen};

public class Atom : MonoBehaviour {


    /// <summary>
    /// This method converts a hex string to RGBA color
    /// </summary>
    /// <param name="hex">Hex string to be converted into RGBA</param>
    /// <returns>RGBA Color</returns>
    public static Color hexToColor(string hex)
    {
        hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
        hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
        byte a = 255;//assume fully visible unless specified in hex
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        //Only use alpha if the string has enough characters
        if (hex.Length == 8)
        {
            a = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        }
        return new Color32(r, g, b, a);
    }


    public string atomName; //Name of the atom
    public AtomType atomType; // Type of the atom
    public bool isConsumed = false; //if atom has formed a bond
    public string atomId; //Id of the atom
    public int electrons; //Total electrons of the atom
    public int availableReactiveSpots;
    public int totalReactiveSpots;

    public FixedJoint joint; // temporary prototype (Atom could be connected to multiple fixed joints)

    private int connections { get; set; }

    /// <summary>
    /// List of all the atoms with their respective colors
    /// </summary>
    private static Dictionary<AtomType, AtomAndEmissionColor> MATERIAL_COLOR = new Dictionary<AtomType, AtomAndEmissionColor>()
    {
        {AtomType.hydrogen, new AtomAndEmissionColor(Color.white, hexToColor("949494"))},
        {AtomType.oxygen, new AtomAndEmissionColor(Color.red, hexToColor("853232"))},
        {AtomType.carbon, new AtomAndEmissionColor(Color.black, hexToColor("2C2C2C"))},
        { AtomType.nitrogen, new AtomAndEmissionColor(Color.cyan, hexToColor("79CDCD"))}
    };

    /// <summary>
    /// This method sets the color of the atom according to its material color
    /// </summary>
    public void SetAtomColor()
    {
        AtomAndEmissionColor atomColor; //variable to get the color of material
        if (MATERIAL_COLOR.TryGetValue(atomType, out atomColor))
        {
            GetComponent<MeshRenderer>().material.color = atomColor.Key; //Atom color chaged
            GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", atomColor.Value);
        }
    }

    public void AddConnection()
    {
        connections++;
    }

    public void RemoveConnection()
    {
        connections--;
        if (connections <= 0)
        {
            transform.parent = null;
        }
    }
}
