﻿using UnityEngine;
using System.Collections;

public class PullAtom : MonoBehaviour
{

    public bool canPull = true;
    public float minPullDistance = 1f;
    public float pullSpeed = 1f;
    public float pullDistance = 20f;

    private LineRenderer laserSight;

    // Use this for initialization
    void Start()
    {
        laserSight = GetComponentInChildren<LineRenderer>();
    }

    void FixedUpdate()
    {
        if (!laserSight.enabled) return;
        Debug.DrawRay(transform.position, -transform.up, Color.cyan, 1f);
        //Debug.Log("Can Pull an atom: " + canPull);

        RaycastHit rayCastHit;

        Physics.Raycast(transform.position, -transform.up, out rayCastHit, pullDistance);

        if (rayCastHit.collider)
        {
            if (rayCastHit.collider.GetComponent<Atom>() && canPull)
            {
                var pulledAtom = rayCastHit.collider.transform;

                    StartCoroutine(StartPulling(pulledAtom));
            }
            else if (rayCastHit.collider.name == "Cylinder") // its molecule connector
            {
                StartCoroutine(StartPulling(rayCastHit.collider.transform.parent));
            }

            //laserSight.SetPosition(1, new Vector3(0, 0, (transform.position - rayCastHit.collider.transform.position).magnitude));
        }

        else
        {
            
        }
    }

    private IEnumerator StartPulling(Transform pulledAtom)
    {
        while (Vector3.Distance(transform.position, pulledAtom.position) > minPullDistance)
        {
            pulledAtom.position = Vector3.Slerp(pulledAtom.position, transform.position, Time.deltaTime * pullSpeed);

            //laserSight.SetPosition(1, new Vector3(0, 0, pulledAtom.position.z));

            yield return 0;
        }
        laserSight.enabled = false;
        yield break;
    }
}
