﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class Molecule : MonoBehaviour {

    public GameObject AtomPrefab;

    public GameObject ConnectionPrefab;

    public List<Atom> atoms = new List<Atom>();
    public bool canReact;

    private readonly Dictionary<string, int> atomsCount = new Dictionary<string, int>();

    private readonly List<Connection> connections = new List<Connection>();

    private GameObject lastCreated;

    #region Unity methods
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    #endregion

    public void AddAtom(string atomSymbol)
    {
        if (!atomsCount.ContainsKey(atomSymbol))
            atomsCount.Add(atomSymbol, 1);
        else
            ++atomsCount[atomSymbol];
        CreateObject(atomSymbol);
    }

    public void AddConnection(GrabbableAtom a, GrabbableAtom b)
    {
        int pos1 = Array.FindIndex(ConfigReader.Atoms, atom => atom == a.ToString());
        int pos2 = Array.FindIndex(ConfigReader.Atoms, atom => atom == b.ToString());

        if (pos1 <= pos2)
            connections.Add(new Connection(a.gameObject, b.gameObject, ConnectionPrefab, this));
        else
            connections.Add(new Connection(b.gameObject, a.gameObject, ConnectionPrefab, this));
    }

    public void DeleteConnection(GameObject toDelete)
    {
        var conn = connections.FirstOrDefault(c => c.CompareCylinder(toDelete));
        if (conn != null)
        {
            conn.Destroy();
            connections.Remove(conn);
        }
    }

    public void DeleteLast()
    {
        if (lastCreated != null)
        {
            string atomSymbol = lastCreated.GetComponentInChildren<TextMesh>().text;
            var atom = lastCreated.GetComponentInChildren<GrabbableAtom>();
            DeleteAllConnections(atom);
            Destroy(lastCreated);
            if (--atomsCount[atomSymbol] == 0)
                atomsCount.Remove(atomSymbol);
            lastCreated = null;
        }
    }

    public void DeleteAll()
    {
        foreach (var conn in connections)
            conn.Destroy();
        foreach (var atom in GetComponentsInChildren<GrabbableAtom>().Select(a => a.gameObject))
            Destroy(atom);
        connections.Clear();
        atomsCount.Clear();
    }

    public void DeleteAllConnections(GrabbableAtom atom)
    {
        var obj = atom.gameObject;
        var toDelete = connections.Where(c => c.FirstAtom == obj || c.SecondAtom == obj).ToList();
        foreach (var conn in toDelete)
        {
            conn.Destroy();
            connections.Remove(conn);
        }
    }

    public Dictionary<string, int> Serialize()
    {
        var atoms = atomsCount.Select(pair => pair.Key + " " + pair.Value.ToString());
        var conns = connections.Select(conn => conn.FirstAtom.GetComponent<GrabbableAtom>().ToString() + " " +
                        conn.SecondAtom.GetComponent<GrabbableAtom>().ToString());
        return atoms.Concat(conns).GroupBy(s => s).ToDictionary(group => group.Key, group => group.Count());
    }

    private void CreateObject(string atomSymbol)
    {
        lastCreated = Instantiate(AtomPrefab);
        lastCreated.transform.parent = transform;
        lastCreated.transform.localPosition = new Vector3(0, 0, 0);
        lastCreated.GetComponentInChildren<TextMesh>().text = atomSymbol;
        //lastCreated.GetComponent<GrabbableAtom>().molecule = this;
    }
}
