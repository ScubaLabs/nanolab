﻿using UnityEngine;
using System.Collections.Generic;

public class MoleculeEntity : MonoBehaviour {
    public List<Atom> atoms = new List<Atom>();
    public bool canReact = false;
}
