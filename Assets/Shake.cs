﻿using UnityEngine;
using System.Collections;

public class Shake : MonoBehaviour {

    private Vector3 targetOne;
    private Vector3 targetTwo;

    private Vector3 target;
    private Vector3 m_initPosition;

	// Use this for initialization
	void Start () {

        m_initPosition = transform.position;

        targetOne = new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.001f);
        targetTwo = new Vector3(transform.position.x, transform.position.y, transform.position.z + 0.001f);

        target = targetOne;

        StartCoroutine(StartShaking());
	}
	
	// Update is called once per frame
	void Update () {

        transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * 1f);
	}

    private void OnDisable()
    {
        StopCoroutine(this.StartShaking());
        transform.position = m_initPosition;
    }

    private IEnumerator StartShaking()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.03f);
            if (target == targetOne)
                target = targetTwo;

            else
                target = targetOne;

            yield return 0;
        }
    }
}
