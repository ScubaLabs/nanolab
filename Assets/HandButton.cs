﻿using UnityEngine;
using System.Collections;
public class HandButton : MonoBehaviour
{

    #region PublicFields
    public GameObject oxygenButton; //Hydrogen atom creation button
    public GameObject hydrogenButton; //Oxygen atom creation button
    public GameObject carbonButton; //Carbon atom creation button
    public GameObject nitrogenButton; //Nitrogen atom creation button
    public GameObject laserSightButton; //Laser sight toggle button
    public MoleculeVRManager moleculeVRManager; // Molecule VR manager
    public float laserCoolDownTime = 1f; // Time to toggle laser sight again
    public float ButtonCoolDownTime = 1f; //Time to make the drop button active again
    public LineRenderer laserSight; //Laser sight
    public PullAtom pullAtom; // Pull Atom button 
    #endregion

    #region PrivateFields 
    private bool isShowing = false; //True if UI is active
    private bool canDropAtom = true; // True if a button can drop atom
    private float atomDropTime = 0;
    //private bool canToggleLaser = true; //True if user can activate laser
    #endregion

    private void Start()
    {
        //GameObject.FindWithTag("PalmToggle") was sometimes returing null, no idea why?
       // laserSight = GameObject.FindWithTag("PalmToggle").GetComponentInChildren<LineRenderer>(); //Find the laser signt
       // pullAtom = GameObject.FindWithTag("PalmToggle").GetComponent<PullAtom>(); //Pull Atom component
    }

    private void Update()
    {
        if (atomDropTime < ButtonCoolDownTime)
        {
            atomDropTime += Time.deltaTime;
        }

        else
        {
            canDropAtom = true;
        }

    }

    /// <summary>
    /// this function toggles the atom creation UI
    /// </summary>
    /// <param name="collision">rigidbody colliding with the palmButton</param>
    private void OnCollisionEnter(Collision collision)
    {
        #region ToggleHandButtons
        if (collision.collider.CompareTag("PalmToggle")) //Checks if colliding rigidbody is ToggleButton or not
        {
            isShowing = !isShowing; //Toggle showing state of the UI

            oxygenButton.SetActive(isShowing); //Hydrogen atom creation button Toggle
            hydrogenButton.SetActive(isShowing); //Oxygen atom creation button Toggle
            carbonButton.SetActive(isShowing); //Carbon atom creation button Toggle
            nitrogenButton.SetActive(isShowing); //Nitrogen atom creation button Toggle
            laserSightButton.SetActive(isShowing); //Toggle Laser sight button
        }
        #endregion

        #region AtomDroppers
        //Checks if any of the Atom Dropper button is hit
        else if (collision.collider.CompareTag("AtomDropper") && canDropAtom)
        {
            AtomCreator touchedAtomCreator = collision.collider.GetComponent<AtomCreator>();

            Atom atom = new Atom();
            atom.atomType = touchedAtomCreator.atomType;
            atom.atomId = touchedAtomCreator.atomId;

            var newAtomPosition = collision.transform.GetChild(0).transform.position;
            if (touchedAtomCreator)
            {
                moleculeVRManager.dropAtom(atom, newAtomPosition);
                canDropAtom = false;
                atomDropTime = 0;
                //StartCoroutine(ToggleCanDropAtom());
            }
        }
        #endregion

        #region LaserSight
        //Check if the Laser sight button is hit
        else if (collision.collider.CompareTag("LaserSight"))
        {
            pullAtom.canPull = !pullAtom.canPull; //Toggle pull of the laser sight
            laserSight.enabled = pullAtom.canPull; //Toggle laser sight On or Off
           // canToggleLaser = false; //Set laser toggle off
            StartCoroutine(ActivateLaserToggle()); // set laser toggle on after cooldown time
        }
        #endregion
    }

    #region coroutines
    ///// <summary>
    ///// Makes atom dropper buttons active after a certain time period 
    ///// </summary>
    ///// <returns></returns>
    //private IEnumerator ToggleCanDropAtom()
    //{
    //    yield return new WaitForSeconds(ButtonCoolDownTime);
    //    canDropAtom = true;
    //}

    /// <summary>
    /// Makes the laser toggle button active again
    /// </summary>
    /// <returns></returns>
    private IEnumerator ActivateLaserToggle()
    {
        yield return new WaitForSeconds(laserCoolDownTime);
       // canToggleLaser = true;
        //Debug.Log("Coroutine canToggle: " + canToggleLaser);
    }
    #endregion
}