/******************************************************************************\
* Copyright (C) 2012-2016 Leap Motion, Inc. All rights reserved.               *
* Leap Motion proprietary and confidential. Not for distribution.              *
* Use subject to the terms of the Leap Motion SDK Agreement available at       *
* https://developer.leapmotion.com/sdk_agreement, or another agreement         *
* between Leap Motion and you, your company or other organization.             *
\******************************************************************************/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using Leap.Unity.Interaction;
using System.Linq.Expressions;
using System;

/// <summary>
/// Handles progression logic for the Interaction Engine 101.
/// </summary>
public class MoleculeVRManager : MonoBehaviour
{

    #region PUBLIC FIELDS

    [Header("MoleculeVR")]
    public GameObject Molecule;

    [Header("AtomList")]
    public GameObject AtomList;

    public Text ShowHideAtoms;

    [Header("Hands")]
    public GameObject LeftHand;
    public GameObject RightHand;

    [Header("Bond Properties")]
    public float maxBondLengthDifference = 0.01f;
    public float bondFormationSpeed = 0.1f;
    public float bondLength = 0.04f;
    public float atomDistance = 0.05f;

    public float breakForce = 35f;

    private GameObject HandBeingUsedToDragAtom;

    private GameObject ActualPartOfHandBeingUsed;


    #endregion

    #region CONSTANTS

    private const float VR_WAIT_TO_MOVE_DURATION = 1F;     // wait X seconds before re-aligning the VR anchor based on the current HMD position
    private const float BEGINNING_FADE_IN_DURATION = 0.5F; // seconds to fade in
    private const float CEILING_SPAWN_RADIUS = 0.1F;       // spawn blocks randomly within this spherical radius

    #endregion

    #region PRIVATE FIELDS

    // Intro fading / camera placement
    private bool _prepVRAnchorOnStart = true;
    private bool _skipIntroDialogue = false;

    // Exercises
    private int _exampleIdx = -1;

    // Neon sign
    private bool _isIENeonTextVisible = false;

    #endregion

    #region UNITY CALLBACKS

    protected void Start()
    {
        foreach (Transform child in AtomList.transform)
        {
            foreach (Transform btnChild in child)
            {
                if (btnChild.tag == "Button")
                {
                    var callback = btnChild.gameObject.GetComponent<HandCollisionCallbacks>();
                    // callback.OnExitCollisionWithHand += dropAtom;
                }
            }

        }

        AtomList.SetActive(false);
    }

    #endregion

    private bool atomListShown = false;

    public void CreateAtom()
    {
#if UNITY_EDITOR
        Debug.Log("Only once");
#endif
        Molecule.GetComponent<Molecule>().AddAtom("O");
    }

    public void showAtomList()
    {
        if (!AtomList.activeSelf)
        {
            AtomList.SetActive(true);
            ShowHideAtoms.text = "Hide Atoms";
        }
        else
        {
            AtomList.SetActive(false);
            ShowHideAtoms.text = "Show Atoms";
        }
    }

    private void onAtomHandRemoved(GameObject atomRemoved)
    {
        createMolecule(atomRemoved);
    }

    private void dropAtom(GameObject atomTouched)
    {
        var atomType = atomTouched.GetComponent<Atom>();
        dropAtom(atomType);
    }

    public void dropAtom(Text atomName)
    {
        Atom atom = new Atom();
        atom.atomId = atomName.text;
        dropAtom(atom);
    }

    public void dropAtom(Atom atom)
    {
        {
            GameObject atomInstance = Instantiate(Resources.Load(atom.atomId)) as GameObject;

            foreach (Transform child in HandBeingUsedToDragAtom.transform)
            {
                if (child.gameObject.tag == "Palm" || child.tag == "Palm")
                {
                    Vector3 pos = child.position;
                    //Vector3 finalPos = child.position;
                    atomInstance.transform.position = child.position;
                }
            }
            var callbacks = atomInstance.AddComponent<HandCollisionCallbacks>();
            callbacks.OnExitCollisionWithHand += onAtomHandRemoved;

            var atomProperties = atomInstance.GetComponent<Atom>();
            atomProperties.atomType = atom.atomType;
            atomProperties.atomId = atom.atomId;
            atomProperties.SetAtomColor();
        }
    }

    System.Collections.Generic.HashSet<GameObject> grasppedAtoms = new System.Collections.Generic.HashSet<GameObject>();

    System.Collections.Generic.Dictionary<int, GameObject> HandIdToItemGrabbed = new System.Collections.Generic.Dictionary<int, GameObject>();

    private void OnAtomGrasped(InteractionBehaviourBase b )
    {
        // Won't work hand id changes all the time, we need to track somehow if its left hand or right hand but anyways
        //var graspingHands = b.GraspingHands;
        //foreach(int id in graspingHands)
        //{
        //    if(b.IsBeingGraspedByHand(id))
        //    {
        //        HandIdToItemGrabbed[id] = b.gameObject;
        //        Debug.Log("OnAtomGrasped id =" + id + "gameObjName = " + b.gameObject.name);
        //        break;
        //    }
        //}
        grasppedAtoms.Add(b.gameObject);

        // as soon as we have two atoms graspped we need to change the force so we can break it
        if(grasppedAtoms.Count == 2)
        {
            // check if they belong to same molecule

            if (grasppedAtoms.ElementAt(0).transform.parent && grasppedAtoms.ElementAt(1).transform.parent &&
                grasppedAtoms.ElementAt(0).transform.parent.gameObject == grasppedAtoms.ElementAt(1).transform.parent.gameObject &&
                grasppedAtoms.ElementAt(0).transform.parent.gameObject.name.Contains("Molecule"))
            {
                // ideally we want to find a route of this atom to other atom and then apply break force to the joints that connect this atom 
                // for eg: if we have A->B->C->D && B->E->F (think of it as a graph), then if user is pulling A & D, we will find a route from A to D and the apply break force to A's immediate joints and D's immediate joints
                var atom1 = grasppedAtoms.ElementAt(0).GetComponent<Atom>();
                atom1.joint.breakForce = breakForce;

                var atom2 = grasppedAtoms.ElementAt(1).GetComponent<Atom>();
                atom2.joint.breakForce = breakForce;
            }
        }

    }

    private void OnAtomReleased(InteractionBehaviourBase b)
    {
        if (grasppedAtoms.Count == 2)
        {
            // revert back the force, so molecule can't be broken

            // check if they belong to same molecule
            if (grasppedAtoms.ElementAt(0).transform.parent && grasppedAtoms.ElementAt(1).transform.parent &&
                grasppedAtoms.ElementAt(0).transform.parent.gameObject == grasppedAtoms.ElementAt(1).transform.parent.gameObject && 
                grasppedAtoms.ElementAt(0).transform.parent.gameObject.name.Contains("Molecule"))
            {
                var atom1 = grasppedAtoms.ElementAt(0).GetComponent<Atom>();
                atom1.joint.breakForce = Mathf.Infinity;

                var atom2 = grasppedAtoms.ElementAt(1).GetComponent<Atom>();
                atom2.joint.breakForce = Mathf.Infinity;
            }
        }
        grasppedAtoms.Remove(b.gameObject);

        //var graspingHands = b.GraspingHands;
        //foreach (int id in graspingHands)
        //{
        //    if (b.IsBeingGraspedByHand(id))
        //    {
        //        HandIdToItemGrabbed.Remove(id);
        //        Debug.Log("OnAtomReleased id =" + id + "gameObjName = " + b.gameObject.name);
        //        break;
        //    }
        //}
    }

    public void dropAtom(Atom atom, Vector3 newAtomposition)
    {
        {
            GameObject atomInstance = Instantiate(Resources.Load(atom.atomId)) as GameObject;

            atomInstance.transform.position = newAtomposition;

            var callbacks = atomInstance.AddComponent<HandCollisionCallbacks>();
            callbacks.OnExitCollisionWithHand += onAtomHandRemoved;

            var atomInteractionBehaviour = atomInstance.GetComponent<InteractionBehaviour>();

            atomInteractionBehaviour.OnGraspBeginEvent += OnAtomGrasped;
            atomInteractionBehaviour.OnGraspEndEvent += OnAtomReleased;

            var atomProperties = atomInstance.GetComponent<Atom>();
            atomProperties.atomType = atom.atomType;
            atomProperties.atomId = atom.atomId;
            atomProperties.SetAtomColor();
        }
    }

    public void setHandUsedToDragAtom(GameObject hand)
    {
        ActualPartOfHandBeingUsed = hand;
        HandBeingUsedToDragAtom = hand.transform.parent.parent.gameObject;
    }

    public void PinchActivated()
    {
        Debug.Log("Pinch Activated");
    }

    public void PinchDeActivated()
    {
        Debug.Log("Pinch DeActivated");
    }

    private GameObject CylinderToAnimate;
    //private Vector3 TarScale;

    /// <summary>
    /// This method creates a molecule when two atoms react
    /// </summary>
    /// <param name="atom">Atom forming bond</param>
    void createMolecule(GameObject atom)
    {
        var objects = Physics.OverlapSphere(atom.transform.position, 0.09f);

        var atomList = new System.Collections.Generic.List<GameObject>();

        foreach (var atm in objects.Select(o => o.GetComponent<Atom>()).Where(a => a != null && a != atom))
        {
            if(atom != atm.gameObject)
                atomList.Add(atm.gameObject);
        }


        var atomSortedByDistance = atomList.OrderBy(x => Mathf.Abs(Vector3.Distance(x.transform.position, atom.transform.position))).ToList();

        if (atom == null || atomSortedByDistance.Count == 0) return;

        foreach (var atm in atomSortedByDistance)
        {
            if (CanFormBond(atom.GetComponent<Atom>(), atm.GetComponent<Atom>()))
            {
                if (!atom.GetComponent<Shake>())
                    atom.gameObject.AddComponent<Shake>();
                if (!atm.GetComponent<Shake>())
                    atm.gameObject.AddComponent<Shake>();

                StartCoroutine(DelayedMoleculeFormation(atom.GetComponent<Atom>(), atm.GetComponent<Atom>()));

                break;
            }
        }
    }

    /// <summary>
    /// Animates the bond formation process
    /// </summary>
    /// <param name="bondStick">Bond stick needed to be animated</param>
    /// <param name="tarScale">Target Scale of the bondstick</param>
    /// <param name="animSpeed">Animation Speed</param>
    public void AnimateBondFormation(GameObject cylinder, Vector3 tarScale, float animSpeed)
    {
        cylinder.transform.localScale = Vector3.MoveTowards(cylinder.transform.localScale, tarScale, Time.deltaTime * animSpeed);
    }

    /// <summary>
    ///Start the animation for forming bonds
    /// </summary>
    /// <param name="bondStick">Bond stick needed to be animated</param>
    /// <param name="tarScale">Target Scale of the bondstick</param>
    /// <param name="animSpeed">Animation Speed</param>
    /// <returns></returns>
    private IEnumerator StartAnimation(GameObject cylinder, Vector3 tarScale, float animSpeed, GameObject atomOne, GameObject atomTwo)
    {
        if (cylinder) //if cylinder is not null
        {
            if (cylinder.transform) //if cylinder transform is not null
            {
                while (Mathf.Abs(cylinder.transform.localScale.y - tarScale.y) >= maxBondLengthDifference) // True while scale of bond stick is greater than tarscale by a minimum value
                {
                    if (!cylinder || !cylinder.transform)
                        break;

                    AnimateBondFormation(cylinder, tarScale, animSpeed);// call to animation function

                    yield return null;
                }
            }
        }
        

        StopAtomShaking(atomOne, atomTwo);

        //var cylinderQuat = cylinder.transform.localRotation;

        //Matrix4x4 m = Matrix4x4.identity;
        //m.SetTRS(Vector3.zero, cylinderQuat, atomOne.transform.localScale);

        //var pt1 = m.MultiplyPoint3x4(new Vector3(0, .4f, 0));
        //var pt2 = m.MultiplyPoint3x4(new Vector3(0, -.4f, 0));

        //atomOne.transform.localPosition = pt1;
        //atomTwo.transform.localPosition = pt2;

        yield break;
    }

    private void StopAtomShaking(GameObject atomOne, GameObject atomTwo)
    {
        if(atomOne && atomOne.GetComponent<Shake>())
            atomOne.GetComponent<Shake>().enabled = false;
        if (atomTwo && atomTwo.GetComponent<Shake>())
            atomTwo.GetComponent<Shake>().enabled = false;
        if (atomOne && atomOne.GetComponent<Shake>())
            Destroy(atomOne.GetComponent<Shake>(), 0);
        if (atomTwo && atomTwo.GetComponent<Shake>())
            Destroy(atomTwo.GetComponent<Shake>(), 0);
    }

    /// <summary>
    /// This method checks if atoms can form a bond or not
    /// </summary>
    /// <param name="atomOne">First atom included in~~ bond formation</param>
    /// <param name="atomTwo">Second atom included in bond formation</param>
    /// <returns>True only if atoms can form a bond</returns>
    private bool CanFormBond(Atom atomOne, Atom atomTwo)
    {
        //If atoms are already consumed then Atoms cannot form bond
        if (atomOne.isConsumed && atomTwo.isConsumed)
        {
            return false;
        }
        else if(atomOne.availableReactiveSpots > 0 && atomTwo.availableReactiveSpots > 0)
        {
            atomOne.availableReactiveSpots--;
            atomTwo.availableReactiveSpots--;
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// This method brings atoms to the exact position where they can bond
    /// </summary>
    /// <param name="atomOne">Atom one forming bond</param>
    /// <param name="atomTwo">Atom two forming bonds</param>
    /// <param name="cylinder">Connecting cylinder</param>
    /// <returns></returns>
    private IEnumerator SetAtomPosition(Transform atomOne, Transform atomTwo, Transform cylinder)
    {
        while (Vector3.Distance(atomOne.position, atomTwo.position) > atomDistance)
        {
            atomTwo.position = Vector3.MoveTowards(atomTwo.position, atomOne.position, Time.deltaTime * 0.1f);
            yield return 0;
        }

        while (Vector3.Distance(atomOne.position, atomTwo.position) < atomDistance)
        {
            atomTwo.position = Vector3.MoveTowards(atomTwo.position, -atomTwo.forward * 1000f, Time.deltaTime * 0.1f);
            yield return 0;
        }

        cylinder.transform.position = (atomTwo.position - atomOne.position) / 2.0f + atomOne.position;
        cylinder.transform.rotation = Quaternion.FromToRotation(Vector3.up, atomTwo.position - atomOne.position);

        #region JoinAtoms

        cylinder.gameObject.AddComponent<FixedJoint>();
        cylinder.gameObject.AddComponent<FixedJoint>();

        var cylinderJoints = cylinder.GetComponents<FixedJoint>();

        cylinderJoints[0].connectedBody = atomOne.GetComponent<Rigidbody>();
        atomOne.GetComponent<Atom>().joint = cylinderJoints[0];
        //cylinderJoints[0].breakForce = breakForce;

        cylinderJoints[1].connectedBody = atomTwo.GetComponent<Rigidbody>();
        atomTwo.GetComponent<Atom>().joint = cylinderJoints[1];
        //cylinderJoints[1].breakForce = breakForce;
        #endregion

        yield return null;
    }

    private IEnumerator DelayedMoleculeFormation(Atom atomOne, Atom atomTwo)
    {
        yield return 0;

        if (atomOne == null || atomTwo == null) yield return 0;

        Transform atomOneTransform = atomOne.transform; // Transform of first atom
        Transform atomTwoTransform = atomTwo.transform; // Transform of second atom

        GameObject atomOneGameObject = atomOne.gameObject; //GameObject of first atom
        GameObject atomTwoGameObject = atomTwo.gameObject;  //GameObject of second atom

        // if both atoms are ready to form bonds
        //if (CanFormBond(atomOne, atomTwo)) this check is not required (nor should be done again) as DelayedMoleculeFormation will only be called if it already passed 
        // CanFormBond will also change reactiveCount so consecutive CanFormBond can return false
        {
            #region AddBond

            Transform atomOneReference = null, atomTwoReference = null;
            
            GameObject molecule = null;

            if(atomOne.isConsumed)
            {
                if(atomOne.transform.parent) //If an atom is inside a molecule
                molecule = atomOne.transform.parent.gameObject;
            }
            else if (atomTwo.isConsumed)
            {
                if(atomTwo.transform.parent) //if atom two is inside a molecule
                molecule = atomTwo.transform.parent.gameObject;
            }


            if (atomOne.isConsumed)
            {
                atomOneReference = atomOneTransform;
                atomTwoReference = atomTwoTransform;

                atomTwoTransform.LookAt(atomOneTransform.position);
            }
            else
            {
                atomOneReference = atomTwoTransform;
                atomTwoReference = atomOneTransform;

                atomOneTransform.LookAt(atomTwoTransform.position);
            }


            if (molecule == null)
            {
                molecule = Instantiate(Resources.Load("MoleculePrefab")) as GameObject;

                var pos = (atomTwo.transform.position - atomOne.transform.position) / 2.0f + atomTwo.transform.position;
                molecule.transform.position = pos;
                atomTwo.transform.LookAt(atomOne.transform.position);
            }


            //Debug.Log(atomOneReference.name + " " + atomTwoReference.name);

            GameObject cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            cylinder.tag = "Cylinder";
            cylinder.AddComponent<Rigidbody>().useGravity = false;
            cylinder.GetComponent<Collider>().enabled = true;
            cylinder.AddComponent<BreakBond>();
            var dirVector = atomOne.transform.position - atomTwo.transform.position;
            cylinder.transform.position = (atomTwo.transform.position - atomOne.transform.position) / 2.0f + atomOne.transform.position;
            cylinder.transform.rotation = Quaternion.FromToRotation(Vector3.up, atomTwoTransform.position - atomOneTransform.position);

            atomOneReference.transform.parent = molecule.transform;
            atomTwoReference.transform.parent = molecule.transform;

            cylinder.transform.parent = molecule.transform;

            StartCoroutine(SetAtomPosition(atomOneReference, atomTwoReference, cylinder.transform));

            Vector3 localScale = new Vector3(0.01f, bondLength, 0.01f);
            cylinder.transform.localScale = new Vector3(0.01f, 0.001f, 0.01f);

            StartCoroutine(StartAnimation(cylinder, localScale, bondFormationSpeed, atomOneGameObject, atomTwoGameObject));
            #endregion

            #region setAtomsConsumed
            atomOne.isConsumed = true;
            atomTwo.isConsumed = true;

            atomOne.AddConnection();
            atomTwo.AddConnection();
            #endregion
        }
    }

    void Update()
    {
        
    }
}