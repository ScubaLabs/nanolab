﻿using UnityEngine;
using System.Collections;

public class BreakBond : MonoBehaviour {

    public float destroySpeed = 0.05f;

    private Atom atomOne;
    private Atom atomTwo;

    private Vector3 targetScale = new Vector3(0, 0, 0);

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
	}

    private void RemoveAtomsFromMolecule(Transform molecule)
    {
            foreach (Transform child in molecule.GetComponentsInChildren<Transform>())
            {
                child.parent = null;
            }

            Destroy(molecule.gameObject, 0);
    }

    private void OnJointBreak(float breakForce)
    {
        var joints = GetComponents<FixedJoint>();
        GameObject molecule = null;

        foreach(var joint in joints)
        {
            UnityEngine.Assertions.Assert.IsNotNull(joint.connectedBody);
            var rb = joint.connectedBody;
            var go = rb.gameObject;
            UnityEngine.Assertions.Assert.IsNotNull(rb.gameObject.GetComponent<Atom>());
            if (rb.gameObject.transform.parent)
            {
                molecule = rb.gameObject.transform.parent.gameObject;
            }
            rb.gameObject.GetComponent<Atom>().isConsumed = false;
            rb.gameObject.GetComponent<Atom>().availableReactiveSpots++;
            // this if condition is a temporary change
            if(rb.gameObject.GetComponent<Atom>().availableReactiveSpots > rb.gameObject.GetComponent<Atom>().totalReactiveSpots)
            {
                rb.gameObject.GetComponent<Atom>().availableReactiveSpots = rb.gameObject.GetComponent<Atom>().totalReactiveSpots;
            }
            UnityEngine.Assertions.Assert.IsTrue(rb.gameObject.GetComponent<Atom>().availableReactiveSpots <= rb.gameObject.GetComponent<Atom>().totalReactiveSpots);
            rb.gameObject.GetComponent<Atom>().RemoveConnection();

        }


        if (molecule) //Checks if molecule still exists in the scene
        {
            if (molecule.transform) //Checks if the transform of the molecule is accessible
            {
                if (molecule.transform.childCount <= 3) //If molecule has less than or equal to two atoms and one bond as child objects
                    RemoveAtomsFromMolecule(molecule.transform); //removes all the atoms from the molecule and then destroys the molecule
            }
        }

        StartCoroutine(BreakAtomBond());
    }

    private IEnumerator BreakAtomBond()
    {
        foreach (Joint j in GetComponents<Joint>())
        {
            Destroy(j, 0);
        }

        while (Vector3.Distance(transform.localScale, targetScale) >= 0.001f)
        {
            transform.localScale = Vector3.MoveTowards(transform.localScale, targetScale, Time.deltaTime * destroySpeed);
            yield return 0;
        }

        Destroy(gameObject, 0);
    }
}
