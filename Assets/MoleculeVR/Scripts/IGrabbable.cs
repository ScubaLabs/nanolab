﻿using UnityEngine;
using System.Collections;

public interface IGrabbable
{
    void OnGrab();
    void OnRelease();
}
