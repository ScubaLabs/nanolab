﻿using UnityEngine;
using System.Collections;

public class AtomButtonCollisionCallback : MonoBehaviour {

    public PhysicalButton toCall;

    public GameObject stage;

    void OnCollisionEnter(Collision coll)
    {
        // set hand coll.gameObject
        //(MoleculeVRManager)(MoleculeVRMgr).HandBeingUsedToDragAtom = coll.gameObject;
        stage.SendMessage("setHandUsedToDragAtom", coll.gameObject);

        toCall.ReceiveCollisionEnter(coll, GetComponentInChildren<Collider>());
    }
}
