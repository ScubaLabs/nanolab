﻿using UnityEngine;
using System.Collections;

public class BondAnimationTest : MonoBehaviour {

    public float bondFormationSpeed = 4f;
    public Transform targetAtom;
    private Animator m_animator;

    private float distance;
    private MeshRenderer m_bondStick;
    private float m_length;
    private Transform m_parent;
    private Vector3 m_targetScale;
    private bool targetSet = false;
	// Use this for initialization
	void Start () {
        m_bondStick = GetComponentInChildren<MeshRenderer>();
        m_animator = transform.root.GetComponent<Animator>();
        m_parent = transform.parent;
        if(targetAtom != null) { 
            m_parent.LookAt(targetAtom);
            m_targetScale = GetTargetScale(m_bondStick, transform, targetAtom.position);
            //targetSet = true;
            StartCoroutine(StartAnimation());
        }
    }
	
	// Update is called once per frame
	void Update () {
        //if (targetAtom == null)
        //{
        //    return;
        //}
        ////if(!targetSet)
        ////{
        ////    m_parent.LookAt(targetAtom);
        ////    m_targetScale = GetTargetScale(m_bondStick, transform, targetAtom.position);
        ////    targetSet = true;
        ////}

        //if (Input.GetKeyDown(KeyCode.B))
        //{
        //    m_parent.LookAt(targetAtom);
        //    m_targetScale = GetTargetScale(m_bondStick, transform, targetAtom.position);
        //}

        //AnimateToTargetScale();
	}

    /// <summary>
    /// Calculates the scale required to set the length of the bondStick equal to required bond length
    /// </summary>
    /// <returns>Target scale of the bond stick</returns>
    public Vector3 GetTargetScale(MeshRenderer m_renderer, Transform m_transform, Vector3 tarPosition)
    {
        float targetScale = 0.0f;
        float p_distance = (m_transform.position - tarPosition).magnitude;
        float p_length = m_renderer.bounds.size.y;
        //float m_angle = Vector3.Angle(m_transform.position, tarPosition);
        //var m_dir = (m_transform.position - tarPosition).normalized;
        targetScale = ((p_distance / (p_length)) * m_transform.localScale.y) * 1.2f;

        //Debug.Log(m_angle);

        //transform.localScale = new Vector3(transform.localScale.x, m_requiredScale, transform.localScale.z);
        return new Vector3(m_transform.localScale.x, targetScale, m_transform.localScale.z); ;
    }

    public void AnimateToTargetScale()
    {
        transform.localScale = Vector3.Slerp(transform.localScale, m_targetScale, Time.deltaTime * bondFormationSpeed);
    } 

    private IEnumerator StartAnimation()
    {
        Debug.Log(transform.localScale.y + " " + m_targetScale.y + " " + Mathf.Abs(transform.localScale.y - m_targetScale.y));
        m_animator.SetBool("isFormingBond", true);

        yield return new WaitForSeconds(3f);

        m_animator.SetBool("isFormingBond", false);

        while (Mathf.Abs(transform.localScale.y - m_targetScale.y) >= 0.01f)
        {
            AnimateToTargetScale();
            Debug.Log(transform.localScale.y + " " + m_targetScale.y);
            Debug.Log(transform.localScale.y - m_targetScale.y);
            yield return null;
        }

        
        Debug.Log("Animation Stopped");
    }
}
