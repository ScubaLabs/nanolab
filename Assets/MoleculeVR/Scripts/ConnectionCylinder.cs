﻿using UnityEngine;
using System.Collections;
using System;

public class ConnectionCylinder : MonoBehaviour, IGrabbable
{
    public Molecule molecule;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnGrab()
    {
        molecule.DeleteConnection(gameObject);
    }

    public void OnRelease()
    {
    }
}
