﻿/******************************************************************************\
* Copyright (C) Leap Motion, Inc. 2011-2014.                                   *
* Leap Motion proprietary. Licensed under Apache 2.0                           *
* Available at http://www.apache.org/licenses/LICENSE-2.0.html                 *
\******************************************************************************/

using UnityEngine;
using System.Collections;
using System.Linq;

public sealed class GrabbableAtom : MonoBehaviour, IGrabbable
{
    public Molecule molecule;

    public bool Grabbed { get { return grabbed; } }
    private bool grabbed;

    private const float connectionRadius = 3.0f;

    public void OnGrab()
    {
        //molecule.DeleteAllConnections(this);
        grabbed = true;
    }

    public void OnRelease()
    {
        CreateConnections();
        grabbed = false;
    }

    public override string ToString()
    {
        return gameObject.GetComponentInChildren<TextMesh>().text;
    }

    private void CreateConnections()
    {
        var objects = Physics.OverlapSphere(transform.position, connectionRadius);
        foreach (var atom in objects.Select(o => o.GetComponent<GrabbableAtom>()).Where(a => a != null && a != this))
            molecule.AddConnection(this, atom);
    }
}
