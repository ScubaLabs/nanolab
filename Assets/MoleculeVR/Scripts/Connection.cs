﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Connection
{
    public GameObject FirstAtom { get { return firstAtom; } }
    private readonly GameObject firstAtom;

    public GameObject SecondAtom { get { return secondAtom; } }
    private readonly GameObject secondAtom;

    private readonly GameObject cylinder;

    private const float cylinderWidth = 0.5f;

    public Connection(GameObject firstAtom, GameObject secondAtom, GameObject prefab, Molecule molecule)
    {
        this.firstAtom = firstAtom;
        this.secondAtom = secondAtom;
        this.firstAtom.GetComponent<SpringJoint>().spring = .1f;
        this.secondAtom.GetComponent<SpringJoint>().spring = .1f;
        this.firstAtom.GetComponent<SpringJoint>().connectedBody = this.secondAtom.GetComponent<Rigidbody>();
        this.secondAtom.GetComponent<SpringJoint>().connectedBody = this.firstAtom.GetComponent<Rigidbody>();
        //this.cylinder = CreateCylinder(prefab, firstAtom.transform.position, secondAtom.transform.position, firstAtom.transform.parent);
        //cylinder.GetComponent<ConnectionCylinder>().molecule = molecule;
    }

    public bool CompareCylinder(GameObject conn)
    {
        return conn.Equals(cylinder);
    }

    public void Destroy()
    {
        Object.Destroy(cylinder);
    }

    private static GameObject CreateCylinder(GameObject prefab, Vector3 startPoint, Vector3 endPoint, Transform parent)
    {
        var offset = endPoint - startPoint;
        var scale = new Vector3(cylinderWidth, offset.magnitude / 2, cylinderWidth);
        var position = startPoint + offset / 2;

        var cylinder = (GameObject)Object.Instantiate(prefab, position, Quaternion.identity);
        cylinder.transform.parent = parent;
        cylinder.transform.up = offset;
        cylinder.transform.localScale = scale;

        return cylinder;
    }
}
